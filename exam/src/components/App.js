import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import Group from './Group'
import AddGroup from './AddGroup'

class App extends PureComponent {
    render() {
        const groups = this.props.groups.sort((a,b)=>(a.title.localeCompare(b.title))).map((group, index) => (<Group key={index} index={index} {...group} />));
        return (
            <div>
                <h1>Tasks</h1>
                <AddGroup/>
                {groups}
            </div>
        );
    }
}

// groups from redux store is mapped to this.props.groups
export default connect(
    (state) => ({
        groups: state.groups
    })
)(App);
