import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {addTask} from '../actions/task';

class AddTask extends PureComponent {

    onClick = () => {
        const {addTask, group} = this.props;
        // dispatch action
        addTask(this.input.value, group);
        this.input.value = '';
    };

    render() {
        // ref allows to map input element to this.input
        return (
            <li>
                <input type="text" ref={node => this.input = node}/>
                <input type="button" onClick={this.onClick} value="Add"/>
            </li>
        );
    }
}

// addTask action creator is mapped to this.props.addTask
export default connect(null, {addTask})(AddTask);