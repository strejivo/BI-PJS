import React, {PureComponent} from 'react';
import {addGroup} from "../actions/group";
import {connect} from "react-redux";

class AddGroup extends PureComponent {
    onClick = () => {
        const {addGroup} = this.props;
        // dispatch action
        addGroup(this.input.value);
        this.input.value = '';
    };

    render() {
        // ref allows to map input element to this.input
        return (
            <li>
                <input type="text" ref={node => this.input = node}/>
                <input type="button" onClick={this.onClick} value="Add"/>
            </li>
        );
    }
}


// addGroup action creator is mapped to this.props.addTask
export default connect(null, {addGroup})(AddGroup);