import React, {PureComponent} from 'react';
import Task from './Task'
import AddTask from './AddTask'
import {connect} from 'react-redux';
import {deleteGroup} from '../actions/group';

class Group extends PureComponent {
    onDeleteClick = () => {
        const { deleteGroup, index} = this.props;
        // dispatch action
        deleteGroup(index);
    };

    render() {
        // this.props.index contains group id (index in groups array)
        const tasks = this.props.tasks.sort((a,b)=>(a.dueTo.getTime() - b.dueTo.getTime())).map((task, index) => (
            <Task key={index} index={index} group={this.props.index} {...task} />));

        return (
            <div>
                <h2>{this.props.title}</h2>
                <ul>
                    <AddTask group={this.props.index} />
                    {tasks}
                </ul>
                <input type="button" value="Delete" onClick={this.onDeleteClick} />
            </div>
        );
    }
}

// toggleTask and deleteTask action creators are mapped to this.props.toggleTask and this.props.deleteTask
export default connect(null, {deleteGroup})(Group);