import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {toggleTask, deleteTask} from '../actions/task';

class Task extends PureComponent {

    onTaskChange = () => {
        // this.props.group contains group id (index in groups array)
        // this.props.index contains task id (index in task array)
        const { toggleTask, group, index } = this.props;
        // dispatch action
        toggleTask(group, index);
    };

    onDeleteClick = () => {
        const { deleteTask, group, index } = this.props;
        // dispatch action
        deleteTask(group, index);
    };

    render() {
        const { title, done, dueTo } = this.props;
        const style = {
            textDecoration: done ? 'line-through' : 'none',
        };
        let timeLeft = (dueTo.getTime() - Date.now())/(1000*60*60*24);
        let daysLeft;
        if (timeLeft > 0)
            daysLeft = Math.floor(timeLeft);
        else
            daysLeft = Math.ceil(timeLeft);
        return (
            <li>
                <label style={style}>
                    <input type="checkbox" checked={done} onChange={this.onTaskChange} />{title}
                </label>
                <p style={style}>Odevzdat do: {dueTo.getDate() + ". "+(dueTo.getMonth()+1) + ". "+dueTo.getFullYear()}, zbývá {daysLeft} dní!</p>
                <input type="button" value="Delete" onClick={this.onDeleteClick} />
            </li>
        );
    }
}

// toggleTask and deleteTask action creators are mapped to this.props.toggleTask and this.props.deleteTask
export default connect(null, {toggleTask, deleteTask})(Task);