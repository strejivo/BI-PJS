// action creators
export const ADD_GROUP = 'ADD_GROUP';
export const DELETE_GROUP = 'DELETE_GROUP';

export function addGroup(title) {
    return {
        type: ADD_GROUP,
        payload: {
            title
        }
    }
}

export function deleteGroup(groupId) {
    return {
        type: DELETE_GROUP,
        payload: {
            groupId
        }
    }
}