import Task from '../models/Task';
import Group from '../models/Group';
import {ADD_TASK, DELETE_TASK, TOGGLE_TASK} from '../actions/task';
import {ADD_GROUP, DELETE_GROUP} from '../actions/group';

let g1 = new Group('Základní část',);
let g2 = new Group('Bonusová část',);

g1.addTask(new Task('přeškrtnutí splněného úkolu'));
g1.addTask(new Task('přidání nové skupiny'));
g1.addTask(new Task('odstranění skupiny'));
g1.addTask(new Task('přidání a vypisování atributu úkolu dueTo (datum kdy má být úkol dokončen)'));
g1.addTask(new Task('u úkolů vypisovat počet dní do dokončení úkolu'));

g2.addTask(new Task('editace úkolu (názvu)', false, new Date(2018, 4,16,14,0,0,0)));
g2.addTask(new Task('editace skupiny (názvu)', false, new Date (2018, 4, 17,14,0,0,0)));
g2.addTask(new Task('řazení úkolů podle data dokončení'));
g2.addTask(new Task('řazení skupin abecedně'));

const initialState = {
    groups: [g1, g2]
};

export default function taskReducer(state = initialState, action) {
    let groups;

    switch (action.type) {
        case ADD_TASK:
            // add new task to given group and left other groups intact
            groups = state.groups.map(
                (group, groupId) => (groupId === action.payload.groupId
                        ? new Group(group.title, [...group.tasks, new Task(action.payload.title)])
                        : group
                )
            );
            return {groups};

        case DELETE_TASK:
            groups = state.groups.map(
                // removes task (filter group tasks) from given group and left other groups intact
                (group, groupId) => (groupId === action.payload.groupId
                        ? new Group(group.title, group.tasks.filter(
                            (task, taskId) => (taskId !== action.payload.taskId))
                        )
                        : group
                )
            );
            return {groups};

        case TOGGLE_TASK: {
            // update task in given group and left other groups intact
            groups = state.groups.map(
                (group, groupId) => (groupId === action.payload.groupId
                        ? new Group(group.title, group.tasks.map(
                            // update given task and left other tasks intact
                            (task, taskId) => (taskId === action.payload.taskId
                                    ? {...task, done: !task.done}
                                    : task
                            ))
                        )
                        : group
                )
            );
            return {groups};
        }
        case ADD_GROUP: {
            groups = [...state.groups, new Group(action.payload.title, [])];
            return {groups}
        }
        case DELETE_GROUP:{
            groups = state.groups.filter((group, groupId) => groupId !== action.payload.groupId);
            return {groups}
        }
        default:
            return state;
    }
}
