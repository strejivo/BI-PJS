export default class Task {
    constructor(title, done, dueTo) {
        this.title = title;
        this.done = done || false;
        this.dueTo = dueTo || new Date();
    }
}