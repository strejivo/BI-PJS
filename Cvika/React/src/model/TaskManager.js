import Task from "./Task";

function TaskManager(){
    this.tasksMap = new Map();
    this.groupsMap = new Map();
    this.usersMap = new Map();
}

TaskManager.prototype.findTask = function (id) {
    return this.tasksMap.get(id);
};

TaskManager.prototype.findAllTasks = function (userID, done=false) {
    let user = this.usersMap.get(userID);
    let ret = [];
    if (user !== undefined) {
        this.tasksMap.forEach(value => {
            if (value.group.owner === user) ret.push(value);
        })
    }
    return ret;
};
TaskManager.prototype.findMatchingTasks = function (substring, userID = undefined) {
    let ret = [];
    if (userID === undefined) {
        this.tasksMap.forEach(value => {
                if (value.title.includes(substring)) ret.push(value);
            }
        );
        return ret;
    }
    else {
        let user = this.usersMap.get(userID);
        if (user !== undefined) return user.tasks.filter(task=>task.title.contains(substring));
        else return [];
    }
};

TaskManager.prototype.addTask = function (group, title, due_date) {
    let ok = true;
    this.tasksMap.forEach(value => {
        if (value.title === title && value.done === false) {
            ok = false;
        }
    });
    if (ok) {
        let newTask = new Task(title, undefined, false, group, new Date(), due_date);
        this.tasksMap.set(newTask.id, newTask);
    }
};

TaskManager.prototype.deleteTask = function (taskID) {
    if (this.tasksMap.has(taskID)){
        let task = this.tasksMap.get(taskID);
        if (task.done){
            this.tasksMap.delete(taskID);
            task.group.tasks = task.group.tasks.filter(taskInGroup => taskInGroup !== task);
        }
    }
};

TaskManager.prototype.renderToDiv = function(){
    let managerDiv = document.createElement("div");
    for (let user of this.usersMap.values()){
        managerDiv.appendChild(user.renderToDiv());
    }
    return managerDiv;
};

export default TaskManager;