let taskIDCounter = 0;
let taskMap = new Map();


function Task(title, description, done, group, created, due_date){
    this.id = taskIDCounter++;
    this.title = title;
    this.description = description || "";
    this.done = done || false;
    this.group =  group;
    this.group.addTask(this);
    this.created = created;
    this.due_date = due_date || 0;
    taskMap.set(this.id, this);
}
Task.prototype.checkConsistency = function () {
    console.assert(this.group.tasks.includes(this));
};
Task.prototype.toString = function (){
    return "Task \""+this.title+"\","+(this.description !== "" ? " with description: " + this.description : "")+" that is " + (this.done ? "done" : "not done") +
        " belongs to group "+this.group.id+". Task created " + this.created + (this.due_date !== 0 ? " and due date is "+this.due_date : "");
};

Task.prototype.serialize = function () {
    return JSON.stringify(this, ((key, value) =>{
        if (key === "group") return value.id;
        else return value;
    }))
};

/*Task.deserialize = function (json) {
    let simpleObj = JSON.parse(json, (key, value) => {
        if (key === "group")
            return groupMap.get(value);
        else return value;
    });
    return new Task(simpleObj.title, simpleObj.description, simpleObj.done, simpleObj.group, simpleObj.created, simpleObj.due_date);
};*/


Task.prototype.renderToDiv = function(){
    let task = this;
    let taskDiv = document.createElement("div");
    taskDiv.id = "task_"+task.id;
    let taskCheckBox = document.createElement("input");
    let taskText = document.createTextNode("TASK: " + task.id + "- " + task.title);
    taskCheckBox.type = "checkbox";
    if (task.done){
        taskCheckBox.checked = true;
        taskDiv.style = "text-decoration: line-through";
    }
    taskCheckBox.addEventListener("change", function(event){
        event.target.parentNode.style = "text-decoration: " + ( this.checked ? "line-through" : "none");
        task.done = this.checked;
        console.log(task.done);
    });
    taskDiv.appendChild(taskCheckBox);
    taskDiv.appendChild(taskText);
    return taskDiv;
};

export default Task;