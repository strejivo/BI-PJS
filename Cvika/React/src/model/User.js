let userMap = new Map();

function User(email, groups){
    this.email = email;
    this.groups = groups || [];
    userMap.set(this.email, this);
}
User.prototype.toString = function () {
    return this.email + " in groups [" + this.groups.map(x => x.title)+"]";
};
User.prototype.addToGroups = function(group){
    this.groups.push(group);
};
User.prototype.checkConsistency = function () {
    for (let group of this.groups) console.assert(group.owner === this);
};

User.prototype.serialize = function () {
    return JSON.stringify(this, (key, value) => {
        if (key === "groups") {
            let ret = '[ ';
            for (let group of value){
                ret += group.id + ", "
            }
            if (value.length >= 1)
                return ret.substr(0, ret.length - 2) + ' ]';
            else return "[]";
        }
        else return value;
    });
};

User.prototype.renderToDiv = function(){
    let user = this;
    let userDiv = document.createElement("div");
    userDiv.id = "user_"+user.email;
    userDiv.appendChild(document.createTextNode("USER: "+user.email));
    for (let groupIndex in user.groups) {
        let group = user.groups[groupIndex];
        userDiv.appendChild(group.renderToDiv());
    }
    return userDiv;
};

export default User;