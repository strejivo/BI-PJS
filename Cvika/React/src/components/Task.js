import React from "react"

class Task extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isChecked: this.props.instance.done
        };
        this.handleChange = this.handleChange.bind(this);
    }
    render(){
        return (<label>
                    <input id={"task"+this.props.id} type="checkbox" checked={this.state.isChecked} onChange={this.handleChange}/>
                    (TASK id = {this.props.instance.id})
                </label>)
    }

    handleChange(){
        this.setState({ isChecked: !this.state.isChecked});
    }
}

export default Task;