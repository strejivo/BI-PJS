import React from "react"
import UserComponent  from "./User"
import User from "../model/User";
import Group from "../model/Group";
import Task from "../model/Task";

class App extends React.Component{
    constructor(props){
      super(props);
    };
    render(){
        var root = new User("IAmRoot");
        var group1 = new Group("group1", root), group2 = new Group("group2", root);
        var task1 = new Task("task1", "do task1", false, group1, new Date(), new Date());
        var task2 = new Task("task2", "do task2", false, group1, new Date(), new Date());
        var task3 = new Task("task3", "do task3", false, group2, new Date(), new Date());
        var task4 = new Task("task4", "do task4", false, group2, new Date(), new Date());
        return (<div>APP<UserComponent instance={root}/></div>);
    };
}

export default App;