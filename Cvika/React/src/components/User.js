import React from "react"
import Group from "./Group";

class User extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (<div>USER email = {this.props.instance.email}
                    <ul>
                        {this.props.instance.groups.map((group) =>
                        <li key={group.id}><Group instance={group}/></li>)}
                    </ul>
                </div>);
    }
}

export default User;
