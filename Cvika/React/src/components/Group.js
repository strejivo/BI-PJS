import React from "react"
import Task from "./Task";

class Group extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (<div>GROUP id = {this.props.instance.id}
                    <ul>
                    {this.props.instance.tasks.map((task)=>
                        <li key={task.id}><Task instance={task}/></li>
                    )}
                    </ul>
                </div>);
    }
}

export default Group;