let todo = require("./cvi04_oop");
let stuff = [];
let user = new todo.User("example@example");
let group1 = new todo.Group("test", user);
let group2 = new todo.Group("test", user);
let task = new todo.Task("Do PJS", undefined, false, group1, new Date(), new Date());
stuff.push(user, group1, group2, task);
for (let x of stuff) {
    console.log(x.toString());
    x.checkConsistency();
}
try {
    user.groups.pop();
    for (let x of stuff) {
        x.checkConsistency();
    }
} catch (aException){
    console.log(aException);
}
