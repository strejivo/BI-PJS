function isPrime (n){
    if (n <= 1) return undefined;
    if (n === 2) return true;
    for (let i = 3; i <= Math.sqrt(n); i+=2){
        if (!(n%i)) return false;
    }
    return true;
}

function allSmallerPrimes(n){
    if (n <= 2) return [];
    let primes = [2];
    for (let i = 3; i < n; i+=2){
        if (isPrime(i)) primes.push(i);
    }
    return primes;
}

console.log(allSmallerPrimes(100));
console.log("----------------------------------------------------");


function isPalindrome(str){
    let cleanStr = str.replace(/[&\/\\#,+()$~%.'":*?<>{} \-]/g,'').toLowerCase();
    return cleanStr === cleanStr.split('').reverse().join('');
}

function isSentencePalindrome(str){
    return isPalindrome(str);
}

function getPalindromesInSentence(str){
    return str.split(' ').filter(x => isPalindrome(x));
}

console.log(isPalindrome("kr k"));
console.log(isSentencePalindrome("Jelenovi pivo nelej"));
console.log(getPalindromesInSentence("anna ma uzky krk"));
console.log("----------------------------------------------------");

function isKeithNumber(n){
    let digits = n.toString().split('').map(x=> parseInt(x));
    let seq = [];
    let last = digits[digits.length-1];
    for (let i = 0; i < digits.length; i++){
        seq.push(digits[i]);
    }
    while(last < n){
        last = 0;
        for (let i = 1; i <= digits.length; i++){
            last += seq[seq.length-i];
        }
        seq.push(last);
    }
    return (last === n);
}

function allSmallerKeithNumbers(n){
    let knums = [];
    for (let i = 10; i<n; i++){
        if (isKeithNumber(i)) knums.push(i);
    }
    return knums;
}

console.log(isKeithNumber(47));
console.log(isKeithNumber(251133297));
console.log(allSmallerKeithNumbers(100));