function nPow(x, n){
	if (n === 0) return 1;
	let ret = x;
	for (let i = 1; i < Math.abs(n); i++){
		ret *= x;	
	}
	return (n > 0 ? ret : 1 / ret );
}

function sumArg(...args){
	let ret = 0;
	for (let i = 0; i < args.length; i++){
		ret += args[i];
	}
	return ret;
}

function myTestFnc(x){
	if (Number.isInteger(x)){
		console.log(x+" is "+ (x < 0 ? "negative" : "positive") +" integer");
	}
	else console.log(x+" is not a integer");
}

//	----------------------------------------------------------------------------------------
function isLeap(x){
	return Number.isInteger(x) && ((x % 400 === 0) || ((x % 4 === 0) && (x % 100 !== 0)));
}
//	----------------------------------------------------------------------------------------

function firstTenMults(n){
	let res = [];
	for (let i = 1; i <= 10; i++){
		res.push(n*i);
	}
	return res;
}

function multsSmallerThanHunder(n){
	let res = [];
	let tmp = n;
	let i = 0;
	while(tmp < 100){
		res.push(tmp);
		tmp+=n;
	}
	return res;
}

//	----------------------------------------------------------------------------------------
function factRec(n) {
	if(n < 0) return undefined;
	if (n <= 1) return 1;
	return n*factRec(n-1);
}

function factIter(n) {
	if (n < 0) return undefined;
	let ret = 1;
	for (let i = 1; i <= n; i++){
		ret*=i;
	}
	return ret;
}
//	----------------------------------------------------------------------------------------

function createRandomFilledArray (size){
	let ret = [];
	for (let i = 0; i < size; i++){
		ret[i] = Math.floor(Math.random()*100);
	}
	return ret;
}

function removeNaN(arr){
    let ret = [];
    for (let i = 0; i < arr.length; i++){
        if (!Number.isNaN(arr[i]) && Number.isFinite(arr[i])) ret.push(arr[i]);
    }
    return ret;
}

function sortArrayAndRemoveNaN(arr){
	return removeNaN(arr).sort(function (a,b) {
		return a-b;
    });
}
//	----------------------------------------------------------------------------------------
function sortArrayEvenFirst(arr){
	return removeNaN(arr).sort(function (a,b) {
		let aIsEven = a%2 === 0, bIsEven = b%2 === 0;
		if (aIsEven === bIsEven) return a-b;
		if (aIsEven) return -1;
		return 1;
    })
}
//	----------------------------------------------------------------------------------------

// Tests;
console.log(nPow(2,-5));
console.log(sumArg(1, 2, 3));
myTestFnc(-1);
myTestFnc(1);
myTestFnc([]);

console.log(isLeap(2000));
console.log(isLeap(2001));
console.log(isLeap(2100));
console.log(isLeap(2400));

console.log(firstTenMults(1));
console.log(firstTenMults(2));
console.log(firstTenMults(10));

console.log(multsSmallerThanHunder(5));

console.log(factRec(5));
console.log(factIter(5));

console.log(createRandomFilledArray(10));
console.log(sortArrayAndRemoveNaN(createRandomFilledArray(10)));
console.log(sortArrayAndRemoveNaN([1, "ahoj"]));
console.log(sortArrayEvenFirst(createRandomFilledArray(10)));

