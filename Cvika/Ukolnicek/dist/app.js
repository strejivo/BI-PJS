/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/app/cvi06.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/app/cvi06.js":
/*!**************************!*\
  !*** ./src/app/cvi06.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _model_User__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../model/User */ \"./src/model/User.js\");\n/* harmony import */ var _model_Group__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../model/Group */ \"./src/model/Group.js\");\n/* harmony import */ var _model_Task__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/Task */ \"./src/model/Task.js\");\n/* harmony import */ var _model_TaskManager__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/TaskManager */ \"./src/model/TaskManager.js\");\n\n\n\n\n\nlet taskManager = new _model_TaskManager__WEBPACK_IMPORTED_MODULE_3__[\"default\"]();\nlet user1 = new _model_User__WEBPACK_IMPORTED_MODULE_0__[\"default\"](\"example@example\");\ntaskManager.usersMap.set(user1.id, user1);\nlet group1 = new _model_Group__WEBPACK_IMPORTED_MODULE_1__[\"default\"](\"test\", user1);\ntaskManager.groupsMap.set(group1.id, group1);\nlet group2 = new _model_Group__WEBPACK_IMPORTED_MODULE_1__[\"default\"](\"test\", user1);\ntaskManager.groupsMap.set(group2.id, group2);\nlet task1 = new _model_Task__WEBPACK_IMPORTED_MODULE_2__[\"default\"](\"Do PJS\", undefined, false, group1, new Date(), new Date());\nlet task2 = new _model_Task__WEBPACK_IMPORTED_MODULE_2__[\"default\"](\"Procrastinate\", undefined, true, group1, new Date(), new Date());\ntaskManager.tasksMap.set(task1.id, task1);\ntaskManager.tasksMap.set(task2.id, task2);\n\nwindow.addEventListener(\"load\", function (event) {\n    document.getElementById(\"content\").appendChild(taskManager.renderToDiv());\n});\n\n\n//# sourceURL=webpack:///./src/app/cvi06.js?");

/***/ }),

/***/ "./src/model/Group.js":
/*!****************************!*\
  !*** ./src/model/Group.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nlet groupIDCounter = 0;\nlet groupMap = new Map();\n\nfunction Group(title, owner, tasks) {\n    this.id = groupIDCounter++;\n    this.title = title;\n    this.owner = owner;\n    this.owner.addToGroups(this);\n    this.tasks = tasks || [];\n    groupMap.set(this.id, this);\n}\nGroup.prototype.checkConsistency = function () {\n    for (let task of this.tasks) console.assert(task.group === this);\n    console.assert(this.owner.groups.includes(this));\n};\nGroup.prototype.addTask = function (task) {\n    this.tasks.push(task);\n};\nGroup.prototype.toString = function(){\n    return \"Group \" + this.id + \" named \\\"\"+this.title + \"\\\" owned by \"+this.owner.email + \" with tasks [\"+this.tasks.map(task => task.title)+\"]\";\n};\n\nGroup.prototype.serialize = function () {\n    return JSON.stringify(this, (key, value)=>{\n        if (key === \"owner\") return value.id;\n        else if (key === \"tasks\") {\n            let ret = '[ ';\n            for (let task of value){\n                ret += task.id + \", \"\n            }\n            if (value.length >= 1)\n                return ret.substr(0, ret.length - 2) + ' ]';\n            else return \"[]\";\n        }\n        else return value;\n    })\n};\nGroup.deserialize = function (json) {\n\n};\n\nGroup.prototype.renderToDiv = function(){\n    let group = this;\n    let groupDiv = document.createElement(\"div\");\n    groupDiv.id = \"group_\"+group.id;\n    groupDiv.appendChild(document.createTextNode(\"GROUP: \" + group.id));\n    for (let taskIndex in group.tasks){\n        groupDiv.appendChild(group.tasks[taskIndex].renderToDiv());\n    }\n    return groupDiv;\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Group);\n\n//# sourceURL=webpack:///./src/model/Group.js?");

/***/ }),

/***/ "./src/model/Task.js":
/*!***************************!*\
  !*** ./src/model/Task.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nlet taskIDCounter = 0;\nlet taskMap = new Map();\n\n\nfunction Task(title, description, done, group, created, due_date){\n    this.id = taskIDCounter++;\n    this.title = title;\n    this.description = description || \"\";\n    this.done = done || false;\n    this.group =  group;\n    this.group.addTask(this);\n    this.created = created;\n    this.due_date = due_date || 0;\n    taskMap.set(this.id, this);\n}\nTask.prototype.checkConsistency = function () {\n    console.assert(this.group.tasks.includes(this));\n};\nTask.prototype.toString = function (){\n    return \"Task \\\"\"+this.title+\"\\\",\"+(this.description !== \"\" ? \" with description: \" + this.description : \"\")+\" that is \" + (this.done ? \"done\" : \"not done\") +\n        \" belongs to group \"+this.group.id+\". Task created \" + this.created + (this.due_date !== 0 ? \" and due date is \"+this.due_date : \"\");\n};\n\nTask.prototype.serialize = function () {\n    return JSON.stringify(this, ((key, value) =>{\n        if (key === \"group\") return value.id;\n        else return value;\n    }))\n};\n\n/*Task.deserialize = function (json) {\n    let simpleObj = JSON.parse(json, (key, value) => {\n        if (key === \"group\")\n            return groupMap.get(value);\n        else return value;\n    });\n    return new Task(simpleObj.title, simpleObj.description, simpleObj.done, simpleObj.group, simpleObj.created, simpleObj.due_date);\n};*/\n\n\nTask.prototype.renderToDiv = function(){\n    let task = this;\n    let taskDiv = document.createElement(\"div\");\n    taskDiv.id = \"task_\"+task.id;\n    let taskCheckBox = document.createElement(\"input\");\n    let taskText = document.createTextNode(\"TASK: \" + task.id + \"- \" + task.title);\n    taskCheckBox.type = \"checkbox\";\n    if (task.done){\n        taskCheckBox.checked = true;\n        taskDiv.style = \"text-decoration: line-through\";\n    }\n    taskCheckBox.addEventListener(\"change\", function(event){\n        event.target.parentNode.style = \"text-decoration: \" + ( this.checked ? \"line-through\" : \"none\");\n        task.done = this.checked;\n        console.log(task.done);\n    });\n    taskDiv.appendChild(taskCheckBox);\n    taskDiv.appendChild(taskText);\n    return taskDiv;\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Task);\n\n//# sourceURL=webpack:///./src/model/Task.js?");

/***/ }),

/***/ "./src/model/TaskManager.js":
/*!**********************************!*\
  !*** ./src/model/TaskManager.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Task__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Task */ \"./src/model/Task.js\");\n\n\nfunction TaskManager(){\n    this.tasksMap = new Map();\n    this.groupsMap = new Map();\n    this.usersMap = new Map();\n}\n\nTaskManager.prototype.findTask = function (id) {\n    return this.tasksMap.get(id);\n};\n\nTaskManager.prototype.findAllTasks = function (userID, done=false) {\n    let user = this.usersMap.get(userID);\n    let ret = [];\n    if (user !== undefined) {\n        this.tasksMap.forEach(value => {\n            if (value.group.owner === user) ret.push(value);\n        })\n    }\n    return ret;\n};\nTaskManager.prototype.findMatchingTasks = function (substring, userID = undefined) {\n    let ret = [];\n    if (userID === undefined) {\n        this.tasksMap.forEach(value => {\n                if (value.title.includes(substring)) ret.push(value);\n            }\n        );\n        return ret;\n    }\n    else {\n        let user = this.usersMap.get(userID);\n        if (user !== undefined) return user.tasks.filter(task=>task.title.contains(substring));\n        else return [];\n    }\n};\n\nTaskManager.prototype.addTask = function (group, title, due_date) {\n    let ok = true;\n    this.tasksMap.forEach(value => {\n        if (value.title === title && value.done === false) {\n            ok = false;\n        }\n    });\n    if (ok) {\n        let newTask = new _Task__WEBPACK_IMPORTED_MODULE_0__[\"default\"](title, undefined, false, group, new Date(), due_date);\n        this.tasksMap.set(newTask.id, newTask);\n    }\n};\n\nTaskManager.prototype.deleteTask = function (taskID) {\n    if (this.tasksMap.has(taskID)){\n        let task = this.tasksMap.get(taskID);\n        if (task.done){\n            this.tasksMap.delete(taskID);\n            task.group.tasks = task.group.tasks.filter(taskInGroup => taskInGroup !== task);\n        }\n    }\n};\n\nTaskManager.prototype.renderToDiv = function(){\n    let managerDiv = document.createElement(\"div\");\n    for (let user of this.usersMap.values()){\n        managerDiv.appendChild(user.renderToDiv());\n    }\n    return managerDiv;\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (TaskManager);\n\n//# sourceURL=webpack:///./src/model/TaskManager.js?");

/***/ }),

/***/ "./src/model/User.js":
/*!***************************!*\
  !*** ./src/model/User.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nlet userMap = new Map();\n\nfunction User(email, groups){\n    this.email = email;\n    this.groups = groups || [];\n    userMap.set(this.email, this);\n}\nUser.prototype.toString = function () {\n    return this.email + \" in groups [\" + this.groups.map(x => x.title)+\"]\";\n};\nUser.prototype.addToGroups = function(group){\n    this.groups.push(group);\n};\nUser.prototype.checkConsistency = function () {\n    for (let group of this.groups) console.assert(group.owner === this);\n};\n\nUser.prototype.serialize = function () {\n    return JSON.stringify(this, (key, value) => {\n        if (key === \"groups\") {\n            let ret = '[ ';\n            for (let group of value){\n                ret += group.id + \", \"\n            }\n            if (value.length >= 1)\n                return ret.substr(0, ret.length - 2) + ' ]';\n            else return \"[]\";\n        }\n        else return value;\n    });\n};\n\nUser.prototype.renderToDiv = function(){\n    let user = this;\n    let userDiv = document.createElement(\"div\");\n    userDiv.id = \"user_\"+user.email;\n    userDiv.appendChild(document.createTextNode(\"USER: \"+user.email));\n    for (let groupIndex in user.groups) {\n        let group = user.groups[groupIndex];\n        userDiv.appendChild(group.renderToDiv());\n    }\n    return userDiv;\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (User);\n\n//# sourceURL=webpack:///./src/model/User.js?");

/***/ })

/******/ });