import User from "../model/User";
import Group from "../model/Group";
import Task from "../model/Task";
import TaskManager from "../model/TaskManager";

let taskManager = new TaskManager();
let user1 = new User("example@example");
taskManager.usersMap.set(user1.id, user1);
let group1 = new Group("test", user1);
taskManager.groupsMap.set(group1.id, group1);
let group2 = new Group("test", user1);
taskManager.groupsMap.set(group2.id, group2);
let task1 = new Task("Do PJS", undefined, false, group1, new Date(), new Date());
let task2 = new Task("Procrastinate", undefined, true, group1, new Date(), new Date());
taskManager.tasksMap.set(task1.id, task1);
taskManager.tasksMap.set(task2.id, task2);

window.addEventListener("load", function (event) {
    document.getElementById("content").appendChild(taskManager.renderToDiv());
});
