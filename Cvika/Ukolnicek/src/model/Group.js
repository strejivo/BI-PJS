let groupIDCounter = 0;
let groupMap = new Map();

function Group(title, owner, tasks) {
    this.id = groupIDCounter++;
    this.title = title;
    this.owner = owner;
    this.owner.addToGroups(this);
    this.tasks = tasks || [];
    groupMap.set(this.id, this);
}
Group.prototype.checkConsistency = function () {
    for (let task of this.tasks) console.assert(task.group === this);
    console.assert(this.owner.groups.includes(this));
};
Group.prototype.addTask = function (task) {
    this.tasks.push(task);
};
Group.prototype.toString = function(){
    return "Group " + this.id + " named \""+this.title + "\" owned by "+this.owner.email + " with tasks ["+this.tasks.map(task => task.title)+"]";
};

Group.prototype.serialize = function () {
    return JSON.stringify(this, (key, value)=>{
        if (key === "owner") return value.id;
        else if (key === "tasks") {
            let ret = '[ ';
            for (let task of value){
                ret += task.id + ", "
            }
            if (value.length >= 1)
                return ret.substr(0, ret.length - 2) + ' ]';
            else return "[]";
        }
        else return value;
    })
};
Group.deserialize = function (json) {

};

Group.prototype.renderToDiv = function(){
    let group = this;
    let groupDiv = document.createElement("div");
    groupDiv.id = "group_"+group.id;
    groupDiv.appendChild(document.createTextNode("GROUP: " + group.id));
    for (let taskIndex in group.tasks){
        groupDiv.appendChild(group.tasks[taskIndex].renderToDiv());
    }
    return groupDiv;
};

export default Group;