let taskIDCounter = 0;
let groupIDCounter = 0;

let groupMap = new Map();
let taskMap = new Map();
let userMap = new Map();

function Task(title, description, done, group, created, due_date){
    this.id = taskIDCounter++;
    this.title = title;
    this.description = description || "";
    this.done = done || false;
    this.group =  group;
    this.group.addTask(this);
    this.created = created;
    this.due_date = due_date || 0;
    taskMap.set(this.id, this);
}
Task.prototype.checkConsistency = function () {
    console.assert(this.group.tasks.includes(this));
};
Task.prototype.toString = function (){
    return "Task \""+this.title+"\","+(this.description !== "" ? " with description: " + this.description : "")+" that is " + (this.done ? "done" : "not done") +
        " belongs to group "+this.group.id+". Task created " + this.created + (this.due_date !== 0 ? " and due date is "+this.due_date : "");
};

Task.prototype.serialize = function () {
  return JSON.stringify(this, ((key, value) =>{
      if (key === "group") return value.id;
      else return value;
  }))
};

Task.deserialize = function (json) {
    let simpleObj = JSON.parse(json, (key, value) => {
        if (key === "group")
            return groupMap.get(value);
        else return value;
    });
    return new Task(simpleObj.title, simpleObj.description, simpleObj.done, simpleObj.group, simpleObj.created, simpleObj.due_date);
};


function Group(title, owner, tasks) {
    this.id = groupIDCounter++;
    this.title = title;
    this.owner = owner;
    this.owner.addToGroups(this);
    this.tasks = tasks || [];
    groupMap.set(this.id, this);
}
Group.prototype.checkConsistency = function () {
    for (let task of this.tasks) console.assert(task.group === this);
    console.assert(this.owner.groups.includes(this));
};
Group.prototype.addTask = function (task) {
    this.tasks.push(task);
};
Group.prototype.toString = function(){
    return "Group " + this.id + " named \""+this.title + "\" owned by "+this.owner.email + " with tasks ["+this.tasks.map(task => task.title)+"]";
};

Group.prototype.serialize = function () {
  return JSON.stringify(this, (key, value)=>{
      if (key === "owner") return value.id;
      else if (key === "tasks") {
          let ret = '[ ';
          for (let task of value){
              ret += task.id + ", "
          }
          if (value.length >= 1)
          return ret.substr(0, ret.length - 2) + ' ]';
          else return "[]";
      }
      else return value;
  })
};
Group.deserialize = function (json) {

};


function User(email, groups){
    this.email = email;
    this.groups = groups || [];
    userMap.set(this.email, this);
}
User.prototype.toString = function () {
    return this.email + " in groups [" + this.groups.map(x => x.title)+"]";
};
User.prototype.addToGroups = function(group){
    this.groups.push(group);
};
User.prototype.checkConsistency = function () {
    for (let group of this.groups) console.assert(group.owner === this);
};

User.prototype.serialize = function () {
    return JSON.stringify(this, (key, value) => {
        if (key === "groups") {
            let ret = '[ ';
            for (let group of value){
                ret += group.id + ", "
            }
            if (value.length >= 1)
                return ret.substr(0, ret.length - 2) + ' ]';
            else return "[]";
        }
        else return value;
    });
};

module.exports = {
    User: User,
    Group: Group,
    Task: Task
};
