function compress(str){
    let charReplacer = function (match){
        return '@'+match.length.toString()+match[0];
    };
    let numReplacer = function (match) {
        return '@'+match.length.toString()+'@'+match[0];
    };
    let charRegExp = /([a-zA-Z])\1{2,}/g;
    let numRegExp = /([0-9])\1{3,}/g;
    return str.replace(charRegExp, charReplacer).replace(numRegExp, numReplacer);
}

function decompress(str){
    let replacer = function(match, p1,p2){
        let ret = "";
        for (let i = parseInt(p1); i > 0; i--){
            ret += p2;
        }
        return ret;
    };

    let charRegExp = /@([0-9]+)([a-zA-Z])/g;
    let numRegExp = /@([0-9])+@([0-9])/g;
    return str.replace(charRegExp, replacer).replace(numRegExp, replacer);
}

console.log(compress("aaaaa444444bbbcd3333225aaa"));
console.log(decompress("@5a@6@4@3bcd@4@3225@3a"));